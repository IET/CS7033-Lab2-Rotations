#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out VS_OUT {
    vec3 Normal;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vs_out;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 modelMat;

uniform vec3 lightPos;
uniform vec3 viewPos;

void main()
{
    gl_Position = projection * view * modelMat * vec4(position, 1.0f);
    vs_out.TangentFragPos = vec3(modelMat * vec4(position, 1.0));   
    vs_out.TexCoords = texCoords;
    
    mat3 normalMatrix = transpose(inverse(mat3(modelMat)));
    vs_out.Normal = normalize(normalMatrix * normal);
    
    vec3 T = normalize(normalMatrix * tangent);
    vec3 B = normalize(normalMatrix * bitangent);
    vec3 N = normalize(normalMatrix * normal);
    //Get inverse of TBN matrix to transform vectors into tangent space
    //Transpose of an orthgonal matrix is equal to its inverse and cheaper to compute
    mat3 TBN = transpose(mat3(T, B, N));
    
    //Transforming all of these into tangent space rather than normals to world
    //space should end up faster because they do not change per-fragment
    vs_out.TangentLightPos = TBN * lightPos;
    vs_out.TangentViewPos  = TBN * viewPos;
    //Exact positiion can be interpolated
    vs_out.TangentFragPos  = TBN * vs_out.TangentFragPos;
    
}