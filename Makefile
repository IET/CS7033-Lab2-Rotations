# Builds all the projects in the solution...
.PHONY: all_projects
all_projects: Animation-Lab2 

# Builds project 'Animation-Lab2'...
.PHONY: Animation-Lab2
Animation-Lab2: 
	$(MAKE) --directory="Animation-Lab2/" --file=Animation-Lab2.makefile

# Cleans all projects...
.PHONY: clean
clean:
	make --directory="Animation-Lab2/" --file=Animation-Lab2.makefile clean

