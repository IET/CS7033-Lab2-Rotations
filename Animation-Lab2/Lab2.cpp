// OGLProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <glfw3.h>

//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Include AntTweakBar
#include <AntTweakBar.h>

//Local Classes
#include "model.h"
#include "ShaderManager.hpp"
#include "Camera.h"

#include "CubeMap.hpp"

#include <string>
#include <sstream>

//Target Framerate (will not exceed)
const int TARGET_FPS = 60;

//Handles Program Initialisation 
bool glInit();

//Camera and Input Variables
Camera camera(glm::vec3(0.0f,0.0f, 100.0f));
bool firstPerson = false;
bool thirdPerson = false;

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

//Define panels to be used in AntTweakBar UI
TwBar * EulerGUI;
TwBar * QuaternionGUI;

// Function prototypes
//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

void handleInput();
void drawScene(glm::mat4 v, glm::mat4 p);

//Window For OpenGL App
GLFWwindow* window;
// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;
//Clipping Planes
float near = 0.1f;
float far = 10000.0f;

//For time-variant operations
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat t = 0.0;
GLint nbFrames = 0;

Model plane;
Model prop;

Shader *shader;

CubeMap cubeMap;

int _tmain(int argc, _TCHAR* argv[])
{
		//Initialise OpenGL
	if (!glInit()) {
		std::cout << "Failed to initialise OpenGL";
		std::cin;
		return -1;
	}

	// Game loop
    while (!glfwWindowShouldClose(window))
    {
		
		// Set frame time
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
		//Should lock framerate at target framerate
		if(deltaTime < (1.0f/TARGET_FPS))
			continue;
		lastFrame = currentFrame;
    
		nbFrames++;
		//Output frame time to console
		if ( currentFrame - lastFrame >= 1.0 ){ // If last prinf() was more than 1 sec ago

			// printf and reset timer

			printf("%f ms/frame\n", 1000.0/double(nbFrames));

			nbFrames = 0;

			lastFrame += 1.0;
		}

		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
		handleInput();

		//Set BG Color & Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Create camera transformation
        glm::mat4 view;

		if (firstPerson)
		{
			glm::vec3 pos = plane.getLocation() +  (5.0f * glm::vec3(plane.modelMatrix[1])) + (9.0f * glm::vec3(plane.modelMatrix[2])) ;
			//view = glm::lookAt(pos, pos + glm::vec3(plane.modelMatrix[3]), glm::cross(pos + glm::vec3(plane.modelMatrix[3]),  glm::vec3(plane.modelMatrix[2])));
			view = glm::lookAt(pos, pos + glm::vec3(glm::normalize(plane.modelMatrix[2]))* 10.0f,  glm::vec3(glm::normalize(plane.modelMatrix[1])) );

		}
		else  if (thirdPerson){
			camera.Position = (plane.getLocation() + glm::vec3(0.0f, 50.0f, -100.0f));
			camera.Front = glm::normalize(plane.getLocation() - camera.Position);
		}
		else
	        view = camera.GetViewMatrix();
		
		//Create Projection Matrix
		glm::mat4 projection;	
		projection = glm::perspective(camera.Zoom, (float)WIDTH/(float)HEIGHT, near, far);

		prop.rotateBy(glm::vec3(0.0f, 0.0f, -50.0 * deltaTime));

		//Draw scene
		drawScene(view, projection);
		
		// Draw AntTweakBar GUI
		TwDraw();

		// Swap the screen buffers
        glfwSwapBuffers(window);
		t += deltaTime;
	}
	    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
	return 0;
}


void drawScene(glm::mat4 v, glm::mat4 p)
{
	plane.getShader()->enableShader();
	plane.getShader()->setUniformMatrix4fv("projection", p);
	plane.getShader()->setUniformMatrix4fv("view", v);

	plane.draw(v, p);

	plane.getShader()->disableShader();

	//Draw CubeMap
	cubeMap.getShader()->enableShader();
	cubeMap.getShader()->setUniformMatrix4fv("projectionMat", p);
	cubeMap.getShader()->setUniformMatrix4fv("viewMat", v);
	cubeMap.drawSkyBox();
	cubeMap.getShader()->disableShader();
	cubeMap.getShader()->disableShader();
}


bool glInit() {
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    
	//Initialise DevIL image loader
	ilInit();

	// Init GLFW
	if (!glfwInit())
	{
		std::cout << "ERROR: Could not initialise GLFW...";
		return false;
	}

	// Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a GLFWwindow object that we can use for GLFW's functions
    window = glfwCreateWindow(WIDTH, HEIGHT, "Animation Assignment 2", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		std::cout << "ERROR: Could not create winodw...";
		return false;
	}

	glfwMakeContextCurrent(window);

    // Set the required callback functions
    glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//Grab and hide mouse cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//glfwSetCursor(window, cursor);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

    // Define the viewport dimensions
    glViewport(0, 0, WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	shader = ShaderManager::loadShader("normal_mapping");
	plane = Model("../models/plane/F-18.obj");
	prop = Model("../models/propeller/prop.obj");
	prop.setLocation(glm::vec3(-0.0f, 0.0f, 20.0f));
	//prop.translate(glm::vec3(-0.078f, 5.33f, 13.0f));

	plane.setShader(shader);
	prop.setShader(shader);

	plane.AddChild(&prop);

	//cubemap initialisation
	cubeMap.loadCubeMap("../textures/cubemaps/ocean_sky/");
	cubeMap.setShader(ShaderManager::loadShader("skybox")); //TODO - This could definitely be hard-coded...


	return true;
}

// Moves/alters the camera positions based on user input
void handleInput()
{
	if (thirdPerson)
	{
		// Camera controls
		if(keys[GLFW_KEY_W])
		{
			//camera.ProcessKeyboard(FORWARD, deltaTime);
			plane.setLocation(plane.getLocation() + (glm::vec3(0.0f, 0.0f, 1.0f) * deltaTime));
		}
		if(keys[GLFW_KEY_S])
		{
			//camera.ProcessKeyboard(BACKWARD, deltaTime);
		}
		if(keys[GLFW_KEY_A])
		{
			//camera.ProcessKeyboard(LEFT, deltaTime);
			plane.rotateBy(glm::vec3(0.0f, 0.0f, -1.0f * deltaTime));
			plane.rotateBy(glm::vec3(0.0f, -1.0f * deltaTime, 0.0f));
		}
		if(keys[GLFW_KEY_D])
		{
			//camera.ProcessKeyboard(RIGHT, deltaTime);
			plane.rotateBy(glm::vec3(0.0f, 0.0f, 1.0f * deltaTime));
		}
		if(keys[GLFW_KEY_RIGHT])
		{
			//plane.rotateBy(glm::vec3(0.0f, 0.0f, 1.0f * deltaTime));
		}
	}
	else
	{
		// Camera controls
		if(keys[GLFW_KEY_W])
		{
			camera.ProcessKeyboard(FORWARD, deltaTime);
		}
		if(keys[GLFW_KEY_S])
		{
			camera.ProcessKeyboard(BACKWARD, deltaTime);
		}
		if(keys[GLFW_KEY_A])
		{
			camera.ProcessKeyboard(LEFT, deltaTime);

		}
		if(keys[GLFW_KEY_D])
		{
			camera.ProcessKeyboard(RIGHT, deltaTime);
		}

		if(keys[GLFW_KEY_RIGHT])
		{
			plane.rotateBy(glm::vec3(0.0f, 0.0f, 1.0f * deltaTime));
		}
		
		if(keys[GLFW_KEY_LEFT])
		{
			plane.rotateBy(glm::vec3(0.0f, 0.0f, -1.0f * deltaTime));
		}
		
		if(keys[GLFW_KEY_UP])
		{
			plane.rotateBy(glm::vec3(1.0f * deltaTime, 0.0f, 0.0f));
		}

		if(keys[GLFW_KEY_DOWN])
		{
			plane.rotateBy(glm::vec3(-1.0f * deltaTime, 0.0f, 0.0f));
		}

		if(keys[GLFW_KEY_Q])
		{
			plane.rotateBy(glm::vec3(0.0f, 1.0f * deltaTime, 0.0f));
		}

		if(keys[GLFW_KEY_E])
		{
			plane.rotateBy(glm::vec3(0.0f, -1.0f * deltaTime, 0.0f));
		}

		if(keys[GLFW_KEY_F])
		{
			plane.useQuaternion = true;
			firstPerson = !firstPerson;
			keys[GLFW_KEY_F] = false;
		}


		if(keys[GLFW_KEY_G])
		{
			plane.setRotation(glm::vec3(2.15521f,  1.56139f, 4.1547f));
			keys[GLFW_KEY_G] = false;
		}

		if(keys[GLFW_KEY_P])
		{
			std::cout << "Rotation: (" << plane.getRotation().x << ", " << plane.getRotation().y << ", " << plane.getRotation().z << ")" << std::endl;
		}

		if (keys[GLFW_KEY_R])
		{
			plane.modelMatrix = glm::mat4(1);
			plane.useQuaternion = !plane.useQuaternion;
			keys[GLFW_KEY_R] = false;
		}

	}
}

//Is called when a mouse button is pushed
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
		return;
	}
     
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//Had to put in this guard because media keys would crash the application
	if (key > 1024 || key < 0)
		return;

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = (float) xpos;
        lastY = (float) ypos;
        firstMouse = false;
    }

    GLfloat xoffset = (float) xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left
    
    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}	


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset * 0.1);
}