//Courtesy of http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
//using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessary OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>
#include <IL/il.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "mesh.h"
#include "Shader.hpp"

const std::string DEFAULT_TEXTURE_DIRECTORY = "../textures/";

GLint TextureFromFile(const char* path, string directory, bool gamma = false);

class Model 
{
public:
	glm::vec3 location;
	glm::vec3 rotation;

	//glm::mat4 rotationMatrix;
	glm::quat quaternionOrientaton;
	bool useQuaternion;

	glm::mat4 modelMatrix;

	std::vector<Model*> children;

    /*  Model Data */
    std::vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    std::vector<Mesh> meshes;
    string directory;
    bool gammaCorrection;

	glm::vec3 minBounds;
	glm::vec3 maxBounds;

    /*  Functions   */
	//Default constructor
	Model() 
	{
		location = glm::vec3(0, 0, 0);
		rotation = glm::vec3(0, 0, 0);

		this->modelMatrix = glm::mat4();
		ilInit();
		useQuaternion = false;
		quaternionOrientaton = glm::quat();
	}
    // Constructor, expects a filepath to a 3D model.
    Model(string const & path, bool gamma = false) : gammaCorrection(gamma)
    {
		location = glm::vec3(0, 0, 0);
		rotation = glm::vec3(0, 0, 0);
		
		this->modelMatrix = glm::mat4();
        this->loadModel(path);
		ilInit();
		useQuaternion = false;
		quaternionOrientaton = glm::quat();
	}

    // Draws the model, and thus all its meshes
    void draw()
    {
		if (this->shader == NULL)
		{
			//Spam console output...
			cout << "Cannot draw model without shader";
			return;
		}
		//this->translate(location);

		//this->rotate(rotation);

		this->shader->setUniformMatrix4fv("modelMat", this->modelMatrix);
        for(GLuint i = 0; i < this->meshes.size(); i++)
            this->meshes[i].Draw(this->shader);

		this->modelMatrix = glm::mat4();
    }

	// Draws the model, and thus all its meshes
    void draw(glm::mat4 v, glm::mat4 p)
    {
		if (this->shader == NULL)
		{
			//Spam console output...
			cout << "Cannot draw model without shader";
			return;
		}

		if (!useQuaternion)
		{
			this->translate(location);
			this->rotate(rotation);
		}
		this->shader->setUniformMatrix4fv("modelMat", this->modelMatrix);
        for(GLuint i = 0; i < this->meshes.size(); i++)
            this->meshes[i].Draw(this->shader);

		this->shader->disableShader();
		
		for (int i =0; i < children.size(); i++)
		{
			children[i]->getShader()->enableShader();
			children[i]->getShader()->setUniformMatrix4fv("projection", p);
			children[i]->getShader()->setUniformMatrix4fv("view", v);

			glm::mat4 temp = children[i]->modelMatrix;
			children[i]->modelMatrix = this->modelMatrix * children[i]->modelMatrix;
			//children[i]->modelMatrix *= this->modelMatrix;// * children[i]->modelMatrix;

			children[i]->draw(v, p);

			children[i]->modelMatrix = temp;
			children[i]->getShader()->disableShader();
		}

		if (!useQuaternion)
			this->modelMatrix = glm::mat4();
    }

	Shader* getShader() {
		return this->shader;
	}

	glm::mat4 getModelMatrix() {
		return this->modelMatrix;
	}

	glm::vec3 getLocation()
	{
		return this->location;
	}

	glm::vec3 getRotation()
	{
		return this->rotation;
	}

	void setShader(Shader* s) {
		this->shader = s;
	}

	void setLocation (glm::vec3 newPos) {
		location = newPos;
	}

	void moveBy(glm::vec3 move) {
		location.x += move.x;
		location.y += move.y;
		location.z += move.z;
	}

	void setRotation(glm::vec3 newRot) {
		rotation = newRot;

		this->modelMatrix = glm::mat4(1);
		if (!useQuaternion)
		{
			//this->modelMatrix = glm::rotate(this->modelMatrix, newRot.x, glm::vec3(1.0f, 0.0f, 0.0f));
			//this->modelMatrix = glm::rotate(this->modelMatrix, newRot.y, glm::vec3(0.0f, 1.0f, 0.0f));
			//this->modelMatrix = glm::rotate(this->modelMatrix, newRot.z, glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else
		{
			this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(newRot), glm::normalize(newRot)));
		}
	}
	
	void rotateBy(glm::vec3 rot) {
		if (!useQuaternion)
		{
			rotation.x += rot.x;
			rotation.y += rot.y;
			rotation.z += rot.z;
		}
		else
		{
			quaternionOrientaton *= glm::angleAxis(glm::length(rot), glm::normalize(rot));
			//glm::mat4 rotMat = glm::mat4_cast(quaternionOrientaton);
			this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(rot), glm::normalize(rot)));
			
			//Ensure location doesn't change (means I don't have to worry about order of operations)
			this->modelMatrix[3] = glm::vec4(location.x, location.y, location.z, 1.0f);
		}
	}

	void rotate(glm::vec3 rotation)
	{
		if (!useQuaternion)
		{
			this->modelMatrix = glm::rotate(this->modelMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			this->modelMatrix = glm::rotate(this->modelMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			this->modelMatrix = glm::rotate(this->modelMatrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else
		{
			this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(rotation), glm::normalize(rotation)));
		}
	}
	void translate(glm::vec3 t)
	{
		this->modelMatrix = glm::translate(this->modelMatrix, t);
	}

	void scale(glm::vec3 s)
	{
		this->modelMatrix = glm::scale(this->modelMatrix, s);
	}
    
	void load(string path)
	{
		this->loadModel(path);
	}

	void AddChild (Model* m)
	{
		children.push_back(m);
	}

private:
	Shader* shader;
    /*  Functions   */
    // Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
    void loadModel(string path)
    {
		minBounds = glm::vec3(100000000.0f, 100000000.0f, 100000000.0f);
        maxBounds = glm::vec3(-100000000.0f, -100000000.0f, -100000000.0f);
        
        // Read file via ASSIMP
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
        // Check for errors
        if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
        {
            std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
            return;
        }
        // Retrieve the directory path of the filepath
        this->directory = path.substr(0, path.find_last_of('/'));

        // Process ASSIMP's root node recursively
        this->processNode(scene->mRootNode, scene);
    }

    // Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(aiNode* node, const aiScene* scene)
    {
        // Process each mesh located at the current node
        for(GLuint i = 0; i < node->mNumMeshes; i++)
        {
            // The node object only contains indices to index the actual objects in the scene. 
            // The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
            this->meshes.push_back(this->processMesh(mesh, scene));			
        }
        // After we've processed all of the meshes (if any) we then recursively process each of the children nodes
        for(GLuint i = 0; i < node->mNumChildren; i++)
        {
            this->processNode(node->mChildren[i], scene);
        }

    }

    Mesh processMesh(aiMesh* mesh, const aiScene* scene)
    {
        // Data to fill
        std::vector<Vertex> vertices;
        std::vector<GLuint> indices;
        std::vector<Texture> textures;

        // Walk through each of the mesh's vertices
        for(GLuint i = 0; i < mesh->mNumVertices; i++)
        {
            Vertex vertex;
            glm::vec3 vector; // We declare a placeholder std::vector since assimp uses its own std::vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
            // Positions
            vector.x = mesh->mVertices[i].x;
            vector.y = mesh->mVertices[i].y;
            vector.z = mesh->mVertices[i].z;
            vertex.Position = vector;

			//Check bounds
			for (int j = 0; j < 3; j++) {
				if (vector[j] < minBounds[j])
					minBounds[j] = vector[j];
				if (vector[j] > maxBounds[j])
					maxBounds[j] = vector[j];
			}
            // Normals
			vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;
            // Texture Coordinates
            if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
            {
                glm::vec2 vec;
                // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
                // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
                vec.x = mesh->mTextureCoords[0][i].x; 
                vec.y = mesh->mTextureCoords[0][i].y;
                vertex.TexCoords = vec;
            }
            else
                vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			
			if (mesh->mTangents != NULL) {
				// Tangent
				vector.x = mesh->mTangents[i].x;
				vector.y = mesh->mTangents[i].y;
				vector.z = mesh->mTangents[i].z;
				vertex.Tangent = vector;
				// Bitangent
				vector.x = mesh->mBitangents[i].x;
				vector.y = mesh->mBitangents[i].y;
				vector.z = mesh->mBitangents[i].z;
				vertex.Bitangent = vector;
			}
			else
			{
				vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);				
				vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
			}
			vertices.push_back(vertex);
        }
        // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
        for(GLuint i = 0; i < mesh->mNumFaces; i++)
        {
            aiFace face = mesh->mFaces[i];
            // Retrieve all indices of the face and store them in the indices std::vector
            for(GLuint j = 0; j < face.mNumIndices; j++)
                indices.push_back(face.mIndices[j]);
        }
        // Process materials
        if(mesh->mMaterialIndex >= 0)
        {
            aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
            // We assume a convention for sampler names in the shaders. Each diffuse texture should be named
            // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
            // Same applies to other texture as the following list summarizes:
            // Diffuse: texture_diffuseN
            // Specular: texture_specularN
            // Normal: texture_normalN

            // 1. Diffuse maps
            std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
            // 2. Specular maps
            std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
            // 3. Normal maps
            vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
            textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
            // 4. Height maps
            vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
            textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
        }
        
        // Return a mesh object created from the extracted mesh data
        return Mesh(vertices, indices, textures);
    }

    // Checks all material textures of a given type and loads the textures if they're not loaded yet.
    // The required info is returned as a Texture struct.
    std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName)
    {
        std::vector<Texture> textures;
        for(GLuint i = 0; i < mat->GetTextureCount(type); i++)
        {
            aiString str;
            mat->GetTexture(type, i, &str);
            // Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
            GLboolean skip = false;
            for(GLuint j = 0; j < textures_loaded.size(); j++)
            {
                if(textures_loaded[j].path == str)
                {
                    textures.push_back(textures_loaded[j]);
                    skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
                    break;
                }
            }
            if(!skip)
            {   // If texture hasn't been loaded already, load it
                Texture texture;
                texture.id = TextureFromFile(str.C_Str(), this->directory);
                texture.type = typeName;
                texture.path = str;
                textures.push_back(texture);
                this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
            }
        }
        return textures;
    }
};

GLint TextureFromFile(const char* path, string directory, bool gamma)
{
     //Generate texture ID and load texture data 
    string filename = string(path);
    filename = directory + '/' + filename;
    GLuint textureID;
    glGenTextures(1, &textureID);
    int width,height;

	ILuint imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT/*IL_ORIGIN_LOWER_LEFT*/);

	if (ilLoadImage((ILstring)filename.c_str())) {
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, gamma ? GL_SRGB : GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);	

		// Parameters
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
	    return textureID;
	} else {
		return -1;
	}
}
