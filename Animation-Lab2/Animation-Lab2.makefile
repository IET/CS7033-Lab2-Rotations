# Compiler flags...
CPP_COMPILER := g++
C_COMPILER := gcc

# Include paths...
Debug_Win32_Include_Path := -I"../../gl_libs/glfw/include/GLFW/" -I"../../gl_libs/AntTweakBar/include" -I"../../gl_libs/devil/include" -I"../../gl_libs/glew/include"
Release_Win32_Include_Path := -I"../../gl_libs/glfw/include/GLFW/" -I"../../gl_libs/AntTweakBar/include" -I"../../gl_libs/devil/include" -I"../../gl_libs/glew/include"

# Library paths...
Debug_Win32_Library_Path := 
Release_Win32_Library_Path := 

# Additional libraries...
Debug_Win32_Libraries := -Wl,--start-group -lassimp -lDevIL -lopengl32 -lglew32s -lglfw3 -lglfw3dll  -Wl,--end-group
Release_Win32_Libraries := -Wl,--start-group -lassimp -lDevIL -lopengl32 -lglew32s -lglfw3 -lglfw3dll  -Wl,--end-group

# Preprocessor definitions...
Debug_Win32_Preprocessor_Definitions := -D GCC_BUILD -D _DEBUG -D _CONSOLE 
Release_Win32_Preprocessor_Definitions := -D GCC_BUILD -D NDEBUG -D _CONSOLE 

# Implictly linked object files...
Debug_Win32_Implicitly_Linked_Objects := 
Release_Win32_Implicitly_Linked_Objects := 

# Compiler flags...
Debug_Win32_Compiler_Flags := -O0 -g 
Release_Win32_Compiler_Flags := -O2 

-include Animation-Lab2.mkdefs

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug_Win32 Release_Win32 

# Builds the Debug_Win32 configuration...
.PHONY: Debug_Win32
Debug_Win32: create_folders  ../gccDebug/Animation-Lab2
../gccDebug/Animation-Lab2: ../gccDebug/Lab2.o ../gccDebug/Shader.o ../gccDebug/ShaderManager.o ../gccDebug/ ../gccDebug/CubeMap.o 
	g++ ../gccDebug/Lab2.o ../gccDebug/Shader.o ../gccDebug/ShaderManager.o ../gccDebug/ ../gccDebug/CubeMap.o  $(Debug_Win32_Library_Path) $(Debug_Win32_Libraries) -Wl,-rpath,../gccDebug -o ../gccDebug/Animation-Lab2

# Compiles file Lab2.cpp for the Debug_Win32 configuration...
-include ../gccDebug/Lab2.d
../gccDebug/Lab2.o: Lab2.cpp
	$(CPP_COMPILER) $(Debug_Win32_Preprocessor_Definitions) $(Debug_Win32_Compiler_Flags) -c Lab2.cpp $(Debug_Win32_Include_Path) -MMD -o ../gccDebug/Lab2.o

# Compiles file Shader.cpp for the Debug_Win32 configuration...
-include ../gccDebug/Shader.d
../gccDebug/Shader.o: Shader.cpp
	$(CPP_COMPILER) $(Debug_Win32_Preprocessor_Definitions) $(Debug_Win32_Compiler_Flags) -c Shader.cpp $(Debug_Win32_Include_Path) -MMD -o ../gccDebug/Shader.o

# Compiles file ShaderManager.cpp for the Debug_Win32 configuration...
-include ../gccDebug/ShaderManager.d
../gccDebug/ShaderManager.o: ShaderManager.cpp
	$(CPP_COMPILER) $(Debug_Win32_Preprocessor_Definitions) $(Debug_Win32_Compiler_Flags) -c ShaderManager.cpp $(Debug_Win32_Include_Path) -MMD -o ../gccDebug/ShaderManager.o

# Compiles file CubeMap.cpp for the Debug_Win32 configuration...
-include ../gccDebug/CubeMap.d
../gccDebug/CubeMap.o: CubeMap.cpp
	$(CPP_COMPILER) $(Debug_Win32_Preprocessor_Definitions) $(Debug_Win32_Compiler_Flags) -c CubeMap.cpp $(Debug_Win32_Include_Path) -MMD -o ../gccDebug/CubeMap.o

# Builds the Release_Win32 configuration...
.PHONY: Release_Win32
Release_Win32: create_folders  ../gccRelease/Animation-Lab2
../gccRelease/Animation-Lab2: ../gccRelease/Lab2.o ../gccRelease/Shader.o ../gccRelease/ShaderManager.o ../gccRelease/ ../gccRelease/CubeMap.o 
	g++ ../gccRelease/Lab2.o ../gccRelease/Shader.o ../gccRelease/ShaderManager.o ../gccRelease/ ../gccRelease/CubeMap.o  $(Release_Win32_Library_Path) $(Release_Win32_Libraries) -Wl,-rpath,C:/Users/dan/iet/CS7033-Animation/CS7033-Lab2-Rotations/gccRelease -o ../gccRelease/Animation-Lab2

# Compiles file Lab2.cpp for the Release_Win32 configuration...
-include ../gccRelease/Lab2.d
../gccRelease/Lab2.o: Lab2.cpp
	$(CPP_COMPILER) $(Release_Win32_Preprocessor_Definitions) $(Release_Win32_Compiler_Flags) -c Lab2.cpp $(Release_Win32_Include_Path) -MMD -o ../gccRelease/Lab2.o

# Compiles file Shader.cpp for the Release_Win32 configuration...
-include ../gccRelease/Shader.d
../gccRelease/Shader.o: Shader.cpp
	$(CPP_COMPILER) $(Release_Win32_Preprocessor_Definitions) $(Release_Win32_Compiler_Flags) -c Shader.cpp $(Release_Win32_Include_Path) -MMD -o ../gccRelease/Shader.o

# Compiles file ShaderManager.cpp for the Release_Win32 configuration...
-include ../gccRelease/ShaderManager.d
../gccRelease/ShaderManager.o: ShaderManager.cpp
	$(CPP_COMPILER) $(Release_Win32_Preprocessor_Definitions) $(Release_Win32_Compiler_Flags) -c ShaderManager.cpp $(Release_Win32_Include_Path) -MMD -o ../gccRelease/ShaderManager.o

# Compiles file CubeMap.cpp for the Release_Win32 configuration...
-include ../gccRelease/CubeMap.d
../gccRelease/CubeMap.o: CubeMap.cpp
	$(CPP_COMPILER) $(Release_Win32_Preprocessor_Definitions) $(Release_Win32_Compiler_Flags) -c CubeMap.cpp $(Release_Win32_Include_Path) -MMD -o ../gccRelease/CubeMap.o

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p ../gccDebug
	mkdir -p ../gccDebug/Animation-Lab2
	mkdir -p ../gccRelease
	mkdir -p ../gccRelease/Animation-Lab2

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f ../gccDebug/*.o
	rm -f ../gccDebug/*.d
	rm -f ../gccDebug/*.a
	rm -f ../gccDebug/*.so
	rm -f ../gccDebug/*.dll
	rm -f ../gccDebug/*.exe
	rm -f ../gccRelease/*.o
	rm -f ../gccRelease/*.d
	rm -f ../gccRelease/*.a
	rm -f ../gccRelease/*.so
	rm -f ../gccRelease/*.dll
	rm -f ../gccRelease/*.exe

